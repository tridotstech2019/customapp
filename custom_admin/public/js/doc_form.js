
$(document).ready(function(){
    $(document).bind('form-refresh', function() {
        let check_doc = cur_frm.doctype;
        if(check_doc=="Kanban Board"){
        	console.log("Kanban Board")
			frappe.ui.form.on('Kanban Board', {
				onload: function(frm) {
					frm.trigger('reference_doctype');
				},
				refresh: function(frm) {
					if(frm.is_new()) return;
					frm.add_custom_button("Show Board", function() {
						frappe.set_route("List", frm.doc.reference_doctype, "Kanban", frm.doc.name);
					});
				},
				reference_doctype: function(frm) {

					// set field options
					if(!frm.doc.reference_doctype) return;

					frappe.model.with_doctype(frm.doc.reference_doctype, function() {
						var options = $.map(frappe.get_meta(frm.doc.reference_doctype).fields,
							function(d) {
								if(d.fieldname && (d.fieldtype === 'Select'||d.fieldname=="employee") &&
									frappe.model.no_value_type.indexOf(d.fieldtype)===-1) {
									return d.fieldname;
								}
								return null;
							});
						frm.set_df_property('field_name', 'options', options);
						frm.get_field('field_name').refresh();
					});
				},
				field_name: function(frm) {
					var field = frappe.meta.get_field(frm.doc.reference_doctype, frm.doc.field_name);
					frm.doc.columns = [];
					field.options && field.options.split('\n').forEach(function(o) {
						o = o.trim();
						if(!o) return;
						var d = frm.add_child('columns');
						d.column_name = o;
					});
					frm.refresh();
				}
			});
		}
		if(check_doc=="Timesheet"){
			console.log("---------console.log")
			
			frappe.ui.form.on('Timesheet Detail', {
				form_render: function(frm, cdt, cdn) {
					frappe.require("assets/custom_admin/css/custom_kanban.css");
                   let row = frappe.get_doc(cdt, cdn);
                   new ChecklistItem({
                            frm: frm.doc,
                            row: row,
                            cdt :cdt,
                             cdn: cdn,
                             item_list: row.checklist_json,
                   })

                }
			})
			var ChecklistItem = Class.extend({
			    init: function(opts) {
			        this.frm = opts.frm;
			        this.row = opts.row;
			        this.cdt = opts.cdt;
			        this.cdn = opts.cdn;
			        this.items_list = opts.item_list;
			        // cur_frm.fields_dict["items"].grid.grid_rows_by_docname[this.cdn].grid_form.fields_dict['add_dispatch'].$wrapper.find('button[data-fieldname="add_dispatch"]').addClass('btn-primary');
			        // cur_frm.fields_dict["items"].grid.grid_rows_by_docname[this.cdn].grid_form.fields_dict['add_dispatch'].$wrapper.find('button[data-fieldname="add_dispatch"]').addClass('btn-sm');
			        this.make();
			    },
			    make: function() {
			    	let me = this;
			    	me.get_items_html(JSON.stringify(me.items_list));
			  //   	let me = this;
			  //   	console.log("--ChecklistItem--")
			  //       cur_frm.fields_dict["time_logs"].grid.grid_rows_by_docname[me.cdn].grid_form.fields_dict['checklist_html'].$wrapper.empty();
			  //       let wrapper = cur_frm.fields_dict["time_logs"].grid.grid_rows_by_docname[me.cdn].grid_form.fields_dict['checklist_html'].$wrapper;
			  //       let items_list = cur_frm.fields_dict["time_logs"].grid.grid_rows_by_docname[me.cdn].grid_form.fields_dict['checklist_json'].get_model_value();
			  //       me.items_list = JSON.parse(items_list);
			  //       let tablehtml = $(`<div id="checklist-div" class="header">
					//   <h2 style="margin:2px;text-align: left;font-size: 15px;">Checklist Item</h2>
					//   <input type="text" id="checklist-input" placeholder="Title...">
					//   <span class="checklist-addBtn">Add</span>
					// </div>

					// <ul id="checklist-ul">
					 
					// </ul>`).appendTo(wrapper);
			  //       console.log(me.items_list) 
			  //      let li_html =""  
			  //       if(me.items_list.length>0){
					// 	me.items_list.forEach(function(d) {
					// 		li_html += "<li data-id="+d.idx+">"+d.name+"</li>"
					// 	});
			  //        }
			  //        $(tablehtml).find('#checklist-ul').append(li_html)
			  //        tablehtml.find('.checklist-addBtn').click(function() {
			  //        	// me.newchecklistElement()
			  //        	var val = $("#checklist-input").val();
			  //        	var idx = me.items_list.length + 1
			  //        	me.items_list.push({"idx":idx, "name":val})
			  //        	me.get_items_html(JSON.stringify(me.items_list));
			  //        })
				     // tablehtml.find('.checklist-addBtn').click(function() {
	        //             let obj = me.items_list.filter(o => o.idx != f.idx);
	        //             $(obj).each(function(k, v) {
	        //                 v.idx = (k + 1);
	        //             })
	        //             me.items_list = obj;
	        //             frappe.model.set_value(me.cdt, me.cdn, "condition_json",JSON.stringify(me.items_list));
	        //             me.get_items_html(JSON.stringify(me.items_list));
	        //         });

			    },
			 get_items_html: function(child_data){
			 	let me = this;
			 	cur_frm.fields_dict["time_logs"].grid.grid_rows_by_docname[me.cdn].grid_form.fields_dict['checklist_html'].$wrapper.empty();
        		let wrapper = cur_frm.fields_dict["time_logs"].grid.grid_rows_by_docname[me.cdn].grid_form.fields_dict['checklist_html'].$wrapper;
        		let tablehtml = $(`<div id="checklist-div" class="header">
					  <span class="control-label" style="margin:2px;text-align: left !important;">Task Items</span>
					  <input class="input-with-feedback form-control" type="text" id="checklist-input" placeholder="Task...">
					  <span class="btn btn-primary btn-sm primary-action checklist-addBtn">Add</span>
					</div>

					<ul id="checklist-ul" style="padding: 8px 12px 8px 12px;">
					 
					</ul>`).appendTo(wrapper);
			 	let no_record = false;
		        if (!child_data || child_data == ''){
		            no_record = true;
		        }
		        else {
		        	
		            let data = eval(JSON.parse(child_data));
		            
		            let li_html =""  
		            if (data && data.length > 0) {
			        	me.items_list = data;
			            data.map(f => {
			            	li_html += '<li style="padding: 6px 5px 8px 35px;" '
			            	if(f.is_completed){
			            		li_html+='class="checked"'
			            	}
			            	li_html +='data-id='+f.idx+'>'+f.name+'<span style="padding: 6px 5px 8px 8px;" class="close">×</span></li>'
			            })
			        }
			        
			        $(wrapper).find('#checklist-ul').append(li_html)
			    }
			    tablehtml.find('.checklist-addBtn').click(function() {
		         	// me.newchecklistElement()
		         	
		         	var val = $("#checklist-input").val();
		         	
		         	me.items_list = eval(JSON.parse(child_data));
		         	var idx = me.items_list.length + 1
		         	
		         	me.items_list.push({"idx":idx, "name":val, "is_completed":0})
		         	frappe.model.set_value(me.cdt, me.cdn, "checklist_json",JSON.stringify(me.items_list));
		         	me.get_items_html(JSON.stringify(me.items_list));
		         })
			    tablehtml.find('li').click(function() {
					// console.log($(this))
					// console.log($(this).attr("data-id"))
					var data_id = $(this).attr("data-id");
					let checked = 0
					$(me.items_list).each(function(k, v) {
                        if(parseInt(v.idx)==parseInt(data_id)){
                        	if(v.is_completed==0){
                        		v.is_completed=1
                        		checked =1
                        	}else{
                        		v.is_completed=0
                        		checked=0
                        	}
                        	
                        }
             
                    })
					// let obj = me.items_list.filter(o => o.idx == data_id);
					// console.log(obj)
					frappe.model.set_value(me.cdt, me.cdn, "checklist_json",JSON.stringify(me.items_list));
					if(checked==1){
						$(this).addClass('checked');
					}else{
						$(this).removeClass('checked');
					}
					

				})
				tablehtml.find('span.close').click(function() {
					
					var cur_idx=$(this).parent().attr("data-id");
					console.log(cur_idx)
					let obj = me.items_list.filter(o => o.idx != cur_idx);
                    // $(obj).each(function(k, v) {
                    //     v.idx = (k + 1);
                    // })
                    me.items_list = obj;
                    frappe.model.set_value(me.cdt, me.cdn, "checklist_json",JSON.stringify(me.items_list));
                    me.get_items_html(JSON.stringify(me.items_list));
					
				})
				 //    var myNodelist = $(tablehtml).find("li");
					// var i;
					// for (i = 0; i < myNodelist.length; i++) {
					//   var span = $(tablehtml).find("span");
					//   console.log(span)

					//   var txt = $(tablehtml).find("\u00D7");
					  
					//   span.className = "close";
					//   span.appendChild(txt);
					//   myNodelist[i].appendChild(span);
					// }
					// Click on a close button to hide the current list item
					var close = $(tablehtml).find("close");
					var i;
					for (i = 0; i < close.length; i++) {
					  close[i].onclick = function() {
					    var div = this.parentElement;
					    div.style.display = "none";
					  }
					}

					// Add a "checked" symbol when clicking on a list item
					
					// $(tablehtml).find('li').click(function() {
					// 	console.log($(this))
					//  	// var list = $(tablehtml).find('ul');
					// // list.addEventListener('click', function(ev) {
					// 	// if (ev.target.tagName === 'LI') {
					// 	    $(this).addClass('checked');
					// 	// }
					// })
					// }, false);
				}

			})
			
		}

	})
})
