# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import json
from frappe import _

@frappe.whitelist()
def quick_kanban_board(doctype, board_name, field_name, project=None):
	'''Create new KanbanBoard quickly with default options'''

	doc = frappe.new_doc('Kanban Board')
	meta = frappe.get_meta(doctype)

	options = ''
	link_option = ''
	for field in meta.fields:
		if field.fieldname == field_name:
			if field.fieldname=="employee":
				link_doc = frappe.db.get_all("Employee", fields=["name", "employee_name"], filters={"status": "Active"})
				link_option = link_doc
			else:
				options = field.options

	columns = []
	if options:
		columns = options.split('\n')
	if link_option:
		for lin in link_option:
			columns.append(lin.name)
	for column in columns:
		if not column:
			continue
		doc.append("columns", dict(
			column_name=column
		))

	doc.kanban_board_name = board_name
	doc.reference_doctype = doctype
	doc.field_name = field_name

	if project:
		doc.filters = '[["Task","project","=","{0}"]]'.format(project)

	if doctype in ['Note', 'ToDo']:
		doc.private = 1

	doc.save()
	return doc

@frappe.whitelist()
def get_data(start_date=None):
	"""Return data to render the warehouse capacity dashboard."""
	capacity_data = get_timesheet_data(start_date)
	return capacity_data



def get_timesheet_data(start_date):
	data = frappe.db.get_all('Employee',
		fields=['*'],
		filters={"status": "Active"}, order_by="name asc")
	for entry in data:
		query = 'select t.*, d.*, tas.subject, t.docstatus as documentstatus, round(t.total_hours,2) as total_hours, round(d.hours,2) as hours from `tabTimesheet` t inner join  `tabTimesheet Detail` d  on t.name=d.parent left join `tabTask` tas  on tas.name=d.task where t.start_date="{start_date}" and t.employee="{employee}" and t.docstatus!=2 order by d.idx desc'.format(start_date=start_date,employee=entry.name)
		frappe.log_error(query, "query")
		timesheet = frappe.db.sql('''{query}'''.format(query=query), as_dict=1)

		entry.update({
			'timesheet': timesheet
		})
		if len(timesheet)>0:
			entry.update({
				'total_hrs': timesheet[0].total_hours,
				'documentstatus': timesheet[0].documentstatus
			})
		else:
			entry.update({
				'total_hrs': 0,
				'documentstatus': 3
			})
	return data


@frappe.whitelist(allow_guest=True)
def insert_doc(doc):
	try:
		from six import string_types
		if isinstance(doc, string_types):
			doc = json.loads(doc)
		if doc.get("parent") and doc.get("parenttype"):
			# inserting a child record
			parent = frappe.get_doc(doc.get("parenttype"), doc.get("parent"))
			parent.append(doc.get("parentfield"), doc)
			parent.save(ignore_permissions=True)
			return parent.as_dict()
		else:
			doc = frappe.get_doc(doc).insert(ignore_permissions=True)
			return doc.as_dict()
	except Exception as e:
		frappe.db.rollback()
		frappe.log_error(frappe.get_traceback(), 'custom_admin.custom_admin.api.insert_doc')

@frappe.whitelist(allow_guest=True)
def update_doc(doc):
	try:
		from six import string_types
		if isinstance(doc, string_types):
			doc = json.loads(doc)
		keys = doc.keys()
		if frappe.db.exists(doc.get('doctype'), doc.get('name')):
			update_doc = frappe.get_doc(doc.get('doctype'), doc.get('name'))
			for key in keys:
				if type(doc.get(key)) != list:
					setattr(update_doc, key, doc.get(key))
			update_doc.save(ignore_permissions=True)
			return update_doc.as_dict()
	except Exception as e:
		frappe.log_error(frappe.get_traceback(),"custom_admin.custom_admin.api.update_doc")

@frappe.whitelist(allow_guest=True)
def login_authentication(username,password,role=None):
	auth = frappe.db.sql('''select `password` from `__Auth`
		where doctype=%(doctype)s and name=%(name)s and fieldname=%(fieldname)s and encrypted=0''',
		{ 'doctype': "User", 'name': username, 'fieldname': "password" },as_dict=1)
	from passlib.context import CryptContext
	passlibctx = CryptContext(
		schemes=[
			"pbkdf2_sha256",
			"argon2",
			"frappe_legacy",
		],
		deprecated=[
			"frappe_legacy",
		],
	)
	if auth:
		if passlibctx.verify(password, auth[0].password):
			user_info = frappe.db.sql(''' SELECT api_key,api_secret FROM `tabUser` WHERE name=%(username)s ''',{'username':username},as_dict=1)
			if user_info:
				roles__ = frappe.db.get_all("Has Role",filters={"parent":username},fields={"role"})
				if role:
					roles=frappe.db.sql("""select * from `tabUser` s inner join `tabHas Role` c on s.name = c.parent where c.role =%(role)s and s.name=%(user)s""",{"user":username,"role":role},as_dict=1) 
					if roles:
						return {"status":"success","roles":roles__,"api_key":user_info[0].api_key,"api_secret":frappe.utils.password.get_decrypted_password("User", username, fieldname='api_secret')} 
				else:
					return {"status":"success","roles":roles__,"api_key":user_info[0].api_key,"api_secret":frappe.utils.password.get_decrypted_password("User", username, fieldname='api_secret')} 
	return {"status":"failed","message":"Invalid Email Id or Password"}

@frappe.whitelist()
def projectbased_task(user):
	try:
		roles = frappe.get_roles(user)
		if roles:
			if "Admin" in roles:
				projects = frappe.db.get_all("Project", fields=["*"], order_by="creation desc")
				if len(projects)>0:
					for project in projects:
						project.tasks = frappe.db.sql(''' SELECT * FROM `tabTask`  WHERE name!="" and project=%(project)s order by creation desc''',{'username':user, 'project':project.name},as_dict=1)
				return {"status": "success", "task": projects}
			if "Employee" in roles: 
				projects = []
				task_projects = frappe.db.sql(''' SELECT t.project FROM `tabTask` t , `tabDocShare` d WHERE t.name!="" and (t.creation=%(username)s or (d.share_name=t.name and d.share_doctype="Task" and d.user=%(username)s)) group by t.project order by t.creation desc''',{'username':user},as_dict=1)
				if len(task_projects)>0:
					project_list = ','.join([('"' + x.project + '"') for x in task_projects])
					projects = frappe.db.sql('''SELECT * from `tabProject` where name in ({0})'''.format(project_list), as_dict=1)
					for project in projects:
						project.tasks = frappe.db.sql(''' SELECT t.* FROM `tabTask` t , `tabDocShare` d WHERE t.name!="" and t.project=%(project)s and (t.creation=%(username)s or (d.share_name=t.name and d.share_doctype="Task" and d.user=%(username)s)) order by t.creation desc''',{'username':user, 'project': project.name},as_dict=1)
				return {"status": "success", "task": projects}
		return {"status": "failed"}
	except Exception:
		frappe.log_error(title="projectbased_task",message=frappe.get_traceback())

@frappe.whitelist()
def get_all_timesheets(start_date=None):
	"""Return all timesheet data based on date filter."""
	data = frappe.db.get_all('Employee',
		fields=['name','status', 'employee_name'],
		filters={"status": "Active"}, order_by="name asc")
	timesheets = []
	for entry in data:
		query = 'select t.*, d.*, tas.subject, t.docstatus as documentstatus, round(t.total_hours,2) as total_hours, round(d.hours,2) as hours from `tabTimesheet` t inner join  `tabTimesheet Detail` d  on t.name=d.parent left join `tabTask` tas  on tas.name=d.task where t.start_date="{start_date}" and t.employee="{employee}" and t.docstatus!=2 order by d.idx desc'.format(start_date=start_date,employee=entry.name)
		timesheet = frappe.db.sql('''{query}'''.format(query=query), as_dict=1)
		if len(timesheet)>0:
			entry.update({
				'timesheet': timesheet,
				'total_hrs': timesheet[0].total_hours,
				'documentstatus': timesheet[0].documentstatus
			})
			timesheets.append(entry)
	return timesheets


""" By Gopi on 7/4/23  """

@frappe.whitelist(allow_guest=True)
def login_user(username,password,role=None):
	try:
		auth = frappe.db.sql('''SELECT `password` FROM `__Auth`
			WHERE doctype=%(doctype)s AND name=%(name)s AND fieldname=%(fieldname)s AND encrypted=0''',
			{ 'doctype': "User", 'name': username, 'fieldname': "password" },as_dict=1)
		from passlib.context import CryptContext
		passlibctx = CryptContext(schemes=["pbkdf2_sha256","argon2","frappe_legacy"],deprecated=["frappe_legacy"])
		if auth:
			if passlibctx.verify(password, auth[0].password):
				user_info = frappe.db.sql(''' SELECT api_key,api_secret FROM `tabUser` WHERE name=%(username)s ''',{'username':username},as_dict=1)
				if user_info:
					role_flag = False
					roles = frappe.db.get_all("Has Role",filters={"parent":username},fields={"role"})
					if role:
						for r in roles:
							if r.role == role:
								role_flag = True
						if role_flag:
							frappe.local.response['roles'] = roles
							frappe.local.response['api_key'] = user_info[0].api_key
							frappe.local.response['api_secret'] = frappe.utils.password.get_decrypted_password("User", username, fieldname='api_secret')
							frappe.local.response['status'] = 'success'
						else:
							frappe.local.response['message'] = 'Given role is not match with the user'
							frappe.local.response['exception'] = 'AuthenticationError'
							frappe.local.response['status'] = 'failed'
					else:
						frappe.local.response['roles'] = roles
						frappe.local.response['api_key'] = user_info[0].api_key
						frappe.local.response['api_secret'] = frappe.utils.password.get_decrypted_password("User", username, fieldname='api_secret')
						frappe.local.response['status'] = 'success'
				else:
					frappe.local.response['message'] = 'User not found'
					frappe.local.response['exception'] = 'AuthenticationError'
					frappe.local.response['status'] = 'failed'
			else:
				frappe.local.response['message'] = 'Incorrect User or Password '
				frappe.local.response['exception'] = 'AuthenticationError'
				frappe.local.response['status'] = 'failed'
		else:
			frappe.local.response['message'] = 'Incorrect User or Password '
			frappe.local.response['exception'] = 'AuthenticationError'
			frappe.local.response['status'] = 'failed'
	except frappe.exceptions.AuthenticationError:
		frappe.log_error(message=frappe.get_traceback(),title='custom_admin.custom_admin.api.login_user')
		frappe.local.response['message'] = 'Incorrect User or Password '
		frappe.local.response['exception'] = 'AuthenticationError'
		frappe.local.response['status'] = 'failed'
	except Exception:
		frappe.log_error(message=frappe.get_traceback(),title='custom_admin.custom_admin.api.login_user')  
		frappe.local.response['message'] = 'Something went wrong'
		frappe.local.response['exception'] = 'Internel server error'
		frappe.local.response['status'] = 'failed'

def strip_html(value):
	try:
		remover = MLRemover()
		remover.feed(value)
		remover.close()
		return remover.get_data()
	except Exception:
		frappe.log_error("custom_admin.custom_admin.api.strip_html",frappe.get_traceback())

from html.parser import HTMLParser
class MLRemover(HTMLParser):
    def __init__(self):
        super().__init__(convert_charrefs=False)
        self.reset()
        self.convert_charrefs = True
        self.fed = []
    def handle_data(self, data):
        self.fed.append(data)
    def handle_entityref(self, name):
        self.fed.append(f'&{name};')
    def handle_charref(self, name):
        self.fed.append(f'&#{name};')
    def get_data(self):
        return ''.join(self.fed)

@frappe.whitelist()
def user_based_task_list(user,page_no=1,page_length=20):
	try:
		start = (int(page_no) - 1) * int(page_length)
		roles = frappe.get_roles(user)
		if roles:
			if "Admin" in roles:
				resp__ = frappe.db.sql(f''' SELECT name,subject,project,status,description,priority,task_weight,exp_start_date,exp_end_date,expected_time FROM `tabTask` ORDER BY creation DESC LIMIT {start},{page_length} ''',as_dict=1)
				for ts in resp__:
					ts['description'] = strip_html(ts.description) if ts.description else ''
				frappe.local.response.status = "success"
				frappe.local.response.message = resp__
			elif "Employee" in roles: 
				resp__ = frappe.db.sql(f''' SELECT name,subject,project,status,description,priority,task_weight,exp_start_date,exp_end_date,expected_time FROM `tabTask`  WHERE owner="{user}" ORDER BY creation DESC LIMIT {start},{page_length} ''',as_dict=1)
				for ts in resp__:
					ts['description'] = strip_html(ts.description) if ts.description else ''
				frappe.local.response.status = "success"	
				frappe.local.response.message = resp__
		else:
			frappe.local.response.status = "failed"
			frappe.local.response.message = "User not having right role to fetch tasks"
	except Exception:
		frappe.local.response.status = "failed"
		frappe.local.response.message = "Something went wrong"
		frappe.log_error(title="custom_admin.custom_admin.api.projectbased_task",message=frappe.get_traceback())

@frappe.whitelist()
def get_timesheet_list(usr,start_date=None,page_no=1,page_length=20):
	try:
		user = usr
		start = (int(page_no) - 1) * int(page_length)
		roles = frappe.get_roles(user)
		if roles:
			condition = ""
			if start_date:
				condition = f" AND T.start_date='{start_date}' "
			if "Admin" in roles:
				data = frappe.db.get_all('Employee',fields=['name','status','employee_name'],filters={"status": "Active"})
				for dd in data:
					if dd.get('status') == "Active":	
						query = f""" SELECT T.name timesheet_id,TD.name timesheet_detail_id,TS.name task_id,T.start_date,T.end_date, TD.activity_type,TD.expected_hours,TD.from_time,TD.to_time,TD.description,
									TD.completed,TD.project,TD.project_name,TD.task,TS.subject, T.docstatus as documentstatus, 
									CASE WHEN T.docstatus = 1 THEN "Submitted" ELSE 'Draft' END AS documentstatus1,
									ROUND(T.total_hours,2) as total_hours, ROUND(TD.hours,2) as hours 
									FROM `tabTimesheet` T INNER JOIN  `tabTimesheet Detail` TD  ON T.name = TD.parent LEFT JOIN `tabTask` TS  ON TS.name = TD.task WHERE 
									T.employee="{dd.name}" AND T.docstatus != 2 {condition} ORDER BY TD.idx DESC LIMIT {start},{page_length} """
						timesheet = frappe.db.sql(query, as_dict=1)
						dd.update({
							'timesheet': timesheet,
							'total_hrs': timesheet[0].total_hours if timesheet else '',
							'documentstatus': timesheet[0].documentstatus if timesheet else '' })
				frappe.local.response.status = "success"
				frappe.local.response.message = data
			elif "Employee" in roles: 
				data = frappe.db.get_all('Employee',fields=['name','status','employee_name'],filters={"status": "Active","user_id":user})
				if data:
					if data[0].get('status') == "Active":	
						query = f""" SELECT T.name timesheet_id,TD.name timesheet_detail_id,TS.name task_id,T.start_date,T.end_date, TD.activity_type,TD.expected_hours,TD.from_time,TD.to_time,TD.description,
									TD.completed,TD.project,TD.project_name,TD.task,TS.subject, T.docstatus as documentstatus, 
									CASE WHEN T.docstatus = 1 THEN "Submitted" ELSE 'Draft' END AS documentstatus1,
									ROUND(T.total_hours,2) as total_hours, ROUND(TD.hours,2) as hours 
									FROM `tabTimesheet` T INNER JOIN  `tabTimesheet Detail` TD  ON T.name = TD.parent LEFT JOIN `tabTask` TS  ON TS.name = TD.task WHERE 
									T.employee="{data[0].name}" AND T.docstatus != 2 {condition} ORDER BY TD.idx DESC LIMIT {start},{page_length} """
						timesheet = frappe.db.sql(query, as_dict=1)
						data[0].update({
							'timesheet': timesheet,
							'total_hrs': timesheet[0].total_hours if timesheet else '',
							'documentstatus': timesheet[0].documentstatus if timesheet else '' })
						frappe.local.response.status = "success"
						frappe.local.response.message = data[0]
					else:
						frappe.local.response.status = "failed"
						frappe.local.response.message = "Employee is not active"
				else:
					frappe.local.response.status = "failed"
					frappe.local.response.message = "Employee not found"
			else:
				frappe.local.response.status = "failed"
				frappe.local.response.message = "User not matched with any role"
		else:
			frappe.local.response.status = "failed"
			frappe.local.response.message = "User not having any role"
	except Exception:
		frappe.local.response.status = "failed"
		frappe.local.response.message = "Something went wrong"
		frappe.log_error(title="custom_admin.custom_admin.api.get_all_timesheets",message=frappe.get_traceback())

@frappe.whitelist()
def insert_document(doc):
	try:
		from six import string_types
		if isinstance(doc, string_types):
			doc = json.loads(doc)
		if doc.get("parent") and doc.get("parenttype"):
			parent = frappe.get_doc(doc.get("parenttype"), doc.get("parent"))
			parent.append(doc.get("parentfield"), doc)
			parent.save(ignore_permissions=True)
			frappe.local.response.status = 'success' 
			frappe.local.response.message = parent.as_dict()
		else:
			doc = frappe.get_doc(doc).insert(ignore_permissions=True)
			frappe.local.response.status = 'success' 
			frappe.local.response.message = doc.as_dict()
	except Exception:
		frappe.db.rollback()
		frappe.local.response.status = 'failed' 
		frappe.local.response.message = 'Somthing went wrong' 
		frappe.log_error(frappe.get_traceback(), 'custom_admin.custom_admin.api.insert_document')

# @frappe.whitelist()
# def update_document(doc):
# 	try:
# 		data = True
# 		child_dic = []
# 		from six import string_types
# 		if isinstance(doc, string_types):
# 			doc = json.loads(doc)
# 		keys = doc.keys()
# 		if doc.get('doctype') == 'Timesheet':
# 			order_details = frappe.db.sql('''select name,creation,employee,start_date,end_date,status from `tabTimesheet` where 
# 											start_date >= %(start_date)s and end_date <= %(end_date)s and status = 'Submitted' 
# 											and employee = %(employee)s ''', {'employee': doc.get('employee'),
# 											'start_date': doc.get('start_date'),'end_date': doc.get('end_date') }, as_dict=1)
# 			if order_details :
# 				frappe.local.response.status = 'failed' 
# 				frappe.local.response.message = 'Document already exists' 
# 				data = False 
# 		if data:
# 			if frappe.db.exists(doc.get('doctype'), doc.get('name')):
# 				update_doc = frappe.get_doc(doc.get('doctype'), doc.get('name'))
# 				for key in keys:
# 					if type(doc.get(key)) != list:
# 						setattr(update_doc, key, doc.get(key))
# 					elif doc.get(key) and type(doc.get(key)) == list:
# 						for each in doc.get(key):
# 							if each.get('name'):
# 								child_dic.append(each)
# 							else:
# 								update_doc.append(key,each)
# 				update_doc.save(ignore_permissions=True)
# 				for each__c in child_dic:
# 					child_doc = frappe.get_doc(each__c.get('doctype'),each__c.get('name'))
# 					child_doc.update(each__c)
# 					child_doc.save(ignore_permissions=True)
# 				update_doc.reload()
# 				frappe.local.response.status = 'success' 
# 				frappe.local.response.message = update_doc.as_dict()
# 			else:
# 				frappe.local.response.status = 'failed' 
# 				frappe.local.response.message = 'Document not exists' 
# 	except Exception:
# 		frappe.db.rollback()
# 		frappe.local.response.status = 'failed' 
# 		frappe.local.response.message = 'Somthing went wrong' 
# 		frappe.log_error(frappe.get_traceback(),"custom_admin.custom_admin.api.update_document")

@frappe.whitelist()
def update_document(doc):
	try:
		child_dic = []
		from six import string_types
		if isinstance(doc, string_types):
			doc = json.loads(doc)
		keys = doc.keys()
		if frappe.db.exists(doc.get('doctype'), doc.get('name')):
			update_doc = frappe.get_doc(doc.get('doctype'), doc.get('name'))
			update_doc.reload()
			for key in keys:
				if type(doc.get(key)) != list:
					setattr(update_doc, key, doc.get(key))
				elif doc.get(key) and type(doc.get(key)) == list:
					for each in doc.get(key):
						if each.get('name'):
							child_dic.append(each)
						else:
							update_doc.append(key,each)
			update_doc.save(ignore_permissions=True)
			# frappe.log_error("11111111child_dic11111111",child_dic)
			for each__c in child_dic:
				child_doc = frappe.get_doc(each__c.get('doctype'),each__c.get('name'))
				# frappe.log_error("1111111child_doc111111111",child_doc)
				child_doc.update(each__c)
				child_doc.save(ignore_permissions=True)
			update_doc.reload()
			frappe.local.response.status = 'success' 
			frappe.local.response.message = update_doc.as_dict()
		else:
			frappe.local.response.status = 'failed' 
			frappe.local.response.message = 'Document not exists' 
	except Exception:
		frappe.db.rollback()
		frappe.local.response.status = 'failed' 
		frappe.local.response.message = 'Somthing went wrong' 
		frappe.log_error(frappe.get_traceback(),"custom_admin.custom_admin.api.update_document")

@frappe.whitelist()
def delete_document(doctype,name,employee):
	try:
		emp_valid = True
		if frappe.db.exists(doctype, name):
			delete_doc = frappe.get_doc(doctype, name)
			if delete_doc.employee == employee:
				emp_valid = True
			else:
				emp_valid = False
			if delete_doc and emp_valid:
				if delete_doc.status == 'Draft':
					frappe.db.sql(f'''DELETE FROM `tab{doctype}` WHERE name = '{name}' ''')
					frappe.db.commit()
					frappe.local.response.status = 'success' 
					frappe.local.response.message = 'Document deleted successfully '
				else:
					frappe.local.response.status = 'failed' 
					frappe.local.response.message = 'Only draft document is deletable'
			else:
				frappe.local.response.status = 'failed' 
				frappe.local.response.message = 'Document is not authorized'
		else:
			frappe.local.response.status = 'failed' 
			frappe.local.response.message = 'Document not exists' 
	except Exception:
		frappe.db.rollback()
		frappe.local.response.status = 'failed' 
		frappe.local.response.message = 'Somthing went wrong' 
		frappe.log_error(frappe.get_traceback(),"custom_admin.custom_admin.api.delete_document")
