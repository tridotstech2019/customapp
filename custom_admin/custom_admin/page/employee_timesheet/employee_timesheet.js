frappe.pages['employee-timesheet'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Employee Timesheet',
		single_column: true
	});
	page.set_secondary_action(__('Refresh'), () => {
        cur_page.page.refresh(wrapper);
    });
	// page.set_secondary_action('Refresh', () => page.timesheet_dashboard.refresh(), 'octicon octicon-sync');
	page.start = 0;

	page.start_date_field = page.add_field({
		fieldname: 'start_date',
		label: __('Timesheet Date'),
		fieldtype: 'Date',
		default: frappe.datetime.nowdate(),
		change: function() {
			page.start = 0;
			cur_page.page.refresh(wrapper);
		}
	});
	// page.end_date_field = page.add_field({
	// 	fieldname: 'end_date',
	// 	label: __('End Date'),
	// 	fieldtype: 'Date',
	// 	default: frappe.datetime.nowdate(),
	// 	change: function() {
	// 		page.start = 0;
	// 		cur_page.page.refresh(wrapper);
	// 	}
	// });
	
};
frappe.pages['employee-timesheet'].refresh = function(wrapper) {
	var start_date = cur_page.page.page.start_date_field.get_value()
	// var end_date = cur_page.page.page.end_date_field.get_value()
    get_timesheet_items(start_date);
}
var get_timesheet_items = function(start_date) {
	console.log("------in--------")
    frappe.call({
        method: 'custom_admin.custom_admin.api.get_data',
        args: {
        	start_date: start_date
        },
        callback: function(data) {
        	
        	if(data.message){
        		console.log(data.message)
        		console.log(cur_page.page.page.main)
        		$('#page-employee-timesheet .page-content').find('.layout-main').find('.timesheet-list-item').empty();
        		$('#page-employee-timesheet .page-content').find('.layout-main').find('.timesheet-list-item').remove();
                // $(cur_page.page.page.main).html(frappe.render_template("timesheet_items", { content: data.message }));
                $(frappe.render_template('timesheet_items',{ data: data.message })).appendTo(cur_page.page.page.main);
        	}
        	}
        })
}