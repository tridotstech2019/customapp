# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "custom_admin"
app_title = "Custom Admin"
app_publisher = "info@valiantsystems.com"
app_description = "Tridots admin system customization"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "info@valiantsystems.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/custom_admin/css/custom_admin.css"
app_include_js = ["/assets/custom_admin/js/custom.js", "/assets/js/custom-admin.min.js"]

# include js, css files in header of web template
# web_include_css = "/assets/custom_admin/css/custom_admin.css"
# web_include_js = "/assets/custom_admin/js/custom_admin.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {"Timesheet" : "public/js/doc_form.js", "Kanban Board" : "public/js/doc_form.js"}
doctype_list_js = {"Timesheet" : "public/js/doc_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}
fixtures = [
	{
		"doctype": "Property Setter",
		"filters": [
			["name", "in", (
					"Task-quick_entry",
					"Task-description-allow_in_quick_entry",
					"Timesheet Detail-to_time-in_list_view",
					"Timesheet-start_date-in_standard_filter",
					"Timesheet-start_date-default"
			)]
		]
	},
	{
		"doctype": "Custom Field",
		"filters": [
			["name", "in", (
					"Timesheet Detail-checklist_html",
					"Timesheet Detail-checklist_json",
					"Timesheet Detail-task_description"
			)]
		]
	}
]
# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "custom_admin.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "custom_admin.install.before_install"
# after_install = "custom_admin.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "custom_admin.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"custom_admin.tasks.all"
# 	],
# 	"daily": [
# 		"custom_admin.tasks.daily"
# 	],
# 	"hourly": [
# 		"custom_admin.tasks.hourly"
# 	],
# 	"weekly": [
# 		"custom_admin.tasks.weekly"
# 	]
# 	"monthly": [
# 		"custom_admin.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "custom_admin.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
	# "frappe.desk.doctype.kanban_board.kanban_board.quick_kanban_board": "custom_admin.custom_admin.api.quick_kanban_board"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "custom_admin.task.get_dashboard_data"
# }

